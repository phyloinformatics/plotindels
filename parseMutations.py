#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Name: parseMutations.py

##
# Import libraries
##

import argparse, re, sys, os.path
from os import path
try:
	from Bio import SeqIO
	from Bio.Seq import Seq
	from Bio.Alphabet import generic_dna
except:
	sys.stderr.write("! ERROR: Could not load Biopython.\n")
	exit()
else:
	# sys.stdout.write("Biopython was loaded.\n")
	pass

##
# Parse arguments from the command line
##

parser=argparse.ArgumentParser()
parser.add_argument("-i", "--input", help = "Input pairwise sequence alignment(s) in FASTA format.", type = str, nargs = "+", required = True)
parser.add_argument("-s", "--source", help = "Code for the source of data ID (default = suffix of the FASTA file).", type = str, nargs = "+", required = False)
parser.add_argument("-g", "--gap", help = "Symbol indicating InDels (the default is '-').", type = str, default = "-", required = False)
parser.add_argument("-m", "--missing", help = "Symbol indicating missing data (the default is '?').", type = str, default = "?", required = False)
parser.add_argument("-r", "--reference", help = "Indicates whether the reference sequence is the first (1) or second (2) entry in the pairwise alignment (default = 1).", type = int, default = 1, choices = [1, 2], required = False)

args = parser.parse_args()

##
# Define functions
##

def removeTrailingGaps(s):
	try:
		i = len(re.compile("^{}+".format(args.gap)).findall(s)[0])
	except:
		i = ""
	try:
		j = len(re.compile("{}+$".format(args.gap)).findall(s)[0])
	except:
		j = ""
	if i:
		s = i * args.missing + s[i : ]
	if j:
		s = s[ : -1 * j] + j * args.missing
	return s

def delGapOnly(sequences):
	blacklist = []
	for p in range(0, len(sequences[1])):
		i = str(sequences[1][p])
		j = str(sequences[2][p])
		if (i == args.gap and j == args.gap) or (i == args.missing and j == args.missing):
			blacklist.append(p)
	for p in sorted(blacklist)[::-1]:
		sequences[1] = sequences[1][:p] + sequences[1][p+1:]
		sequences[2] = sequences[2][:p] + sequences[2][p+1:]
	return sequences

def readFasta(fastaFile):
	sequences = {}
	headers   = {}
	if(path.exists(fastaFile)):
		handle       = open(fastaFile, "r")
		countEntries = 1
		for record in SeqIO.parse(handle, "fasta"):
			headers[countEntries]    = record.description
			sequences[countEntries]  = list(removeTrailingGaps(str(record.seq)))
			# sys.stdout.write(">{}\n{}\n".format(record.description, record.seq))
			countEntries            += 1
		handle.close()
	return delGapOnly(sequences), headers

def maskReferenceSeq(sequence, gap):
	position = 1
	track = {}
	for i in range(0, len(sequence)):
		if(sequence[i]  != gap):
			track[i]     = position
			position    += 1
		else:
			track[i]     = gap
	# sys.stdout.write("//\n{}\n//".format(sequence))
	return track

def translate(seq):
	return str(Seq(re.sub("U","T", "".join(seq).upper()), generic_dna).translate())

def mutations(track, ref, seq, gap):
	codons = []
	codon = []
	for i in sorted(track):
		if track[i] != gap:
			codon.append(i)
		if len(codon) == 3:
			codons += [codon]
			codon = []
	mutations = {}
	for codon in codons:
		c1 = ref[codon[0]] + ref[codon[1]] + ref[codon[2]]
		c2 = seq[codon[0]] + seq[codon[1]] + seq[codon[2]]
		try:
			c1 = translate(c1)
		except:
			c1 = args.missing
		try:
			c2 = translate(c2)
		except:
			c2 = args.missing
		if c1 != args.missing and c2 != args.missing and c2 != c1:
			mutations[track[codon[0]]] = [c1, c2]
	return mutations

def reportMutations(max, mutationsDic, fileNumber, sources):
	for i in range(1, max + 1):
		if(i in mutationsDic):
			sys.stdout.write("{}, {}, {}, {}, {}, {}, {}\n".format(fileNumber, i, sources[fileNumber - 1], "translation", "1", "1", ":".join(mutationsDic[i])))
		else:
			sys.stdout.write("{}, {}, {}, {}, {}, {}, {}\n".format(fileNumber, i, sources[fileNumber - 1], "translation", "0", "0", "NA"))
	return

def getPatientData(fileList):
	sources = []
	for file in fileList:
		try:
			source = re.compile("^(.+?)[\.\_\-].*").findall(re.sub(".*\/", "", file))[0]
		except:
			sys.stderr.write("! ERROR: Problems parsing file name ({}) to extract source data. Make sure the file names start with the source ID followed by a period, an underscore, or a dash.\n".format(file))
			exit()
		else:
			sources.append(source)
	return sources

##
# Execute functions
##

if((not args.source) or (len(args.source) != len(args.input))):
	sources = getPatientData(args.input)
if((not sources) or (len(sources) != len(args.input))):
	sys.stderr.write("! ERROR: Problems parsing source data. Please check arguments or input file names.\n")
	exit()

fileNumber = 0
sys.stdout.write("N, Position, Source, Type, Length, Count, Change\n")
for fastaFile in args.input:
	fileNumber += 1
	sequences, headers = readFasta(fastaFile)
	if(args.reference == 1):
		track              = maskReferenceSeq(sequences[1], args.gap)
		mutationsDic       = mutations(track, sequences[1], sequences[2], args.gap)
	else:
		track              = maskReferenceSeq(sequences[2], args.gap)
		mutationsDic       = mutations(track, sequences[2], sequences[1], args.gap)
	max                = [track[i] for i in track if track[i] != args.gap][-1]
	reportMutations(max, mutationsDic, fileNumber, sources)

exit() # Quit this script
