# Plotting InDels

PlotIndels was developed together with Siwen Wu (@swu22) during her Ph.D. rotation at [Dr. Daniel Janies's Lab](https://janieslab.github.io/index.html) (Department of Bioinformatics and Genomics, University of North Carolina at Charlotte).

## The problem

Take several pairwise alignments, each composed of a nucleotide or amino acid sequence from a new source aligned against the reference sequence. How do the insertions and deletions (InDels, often represented with the gap symbol “-“) are distributed concerning the reference sequence?

## Dependencies

The scripts here were written on **Python** v3.6.8 and **R** v3.5.3. They should work on newer versions of **Python** and **R** as well. On Python, the user will need [*Biopython*](https://biopython.org/) and other modules that should come with Python. On R, the user will need the libraries `ggplot2`,  `scales`,  `gridExtra`, and `optparse`.

## The example

In this example (see the `example` directory), we have pairwise alignments of sequences from different sources in the form of several multi-FASTA files (i.e., each file represents two sequences aligned against each other). In all files, the second entry represents the reference sequence.

### Parsing alignments

We can use the `parseAlignments.py` script to read all the FASTA files in the `example` directory and create a single data table in comma-separated values (`.csv`).

```bash
$ python3 parseAlignments.py -i example/*.fasta -r 2 > data.csv
```

Use the argument `-h` or `--help` to get more information about the command-line arguments for all Python scripts in this project.

#### Naming input files

The script `parseAlignments.py` expects that files containing pairwise alignments are named based on the sequence ID of the sequence that is being aligned to the reference.

#### Trailing gaps and gap-only columns

Trailing gaps will be replavced by missing data and gap-only columns will be removed.

### Plotting InDel information

We can use the `plotIndels.R` script to read the `data.csv` file created above and plot the data into different image files.

```bash
$ Rscript plotIndels.R -c data.csv
```

Use the argument `-h` or `--help` to get more information about the command-line arguments for all R scripts in this project.

#### Additional parameters

See the comments inside the R script to know how to make changes such as size and resolution of images.

### Output

- The scripts described above will result into data tables and image files.
- Examples of these files are provided in the `examples` directory.
- The order of facets is determined by the number of (not length) InDels (descending order from top to bottom).

## Auxiliary scripts

- `splitMultiAlign.py`: This script can help you spliting a multi-FASTA file with a multiple sequence alignment into files containing the reference aligned to one other sequence at a time.
- `plotMutations.R`: Now you can also plot mutations other than InDels using the same output daple produced by the main Python script.
- `parseTranslations.py`: You can use this script to plot changes (except InDels) that affect translation.
- `parseTables.py`: You can use this script to summarize the CSV tables produced with `parseAlignments.py` and `parseTranslations.py`
- `parseSimilarities.py`: This Python script helps you parse alignment information and retrieve identical positions.
- `plotBlocks.R`:  This R script halps you plot information about identical characters and identical strings of characters.
- `plotMutations.R`: We can use the `plotMutations.R` script to read a data table from `parseAlignments.py` and plot information about changes other than InDels.
- `plotSimilarities.R`: Use this script to plot similarities for individual characters.