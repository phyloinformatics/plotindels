#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Name: parseSimilarities.py

##
# Import libraries
##

import argparse, re, sys, os.path
from os import path
try:
	from Bio import SeqIO
except:
	sys.stderr.write("! ERROR: Could not load Biopython.\n")
	exit()
else:
	# sys.stdout.write("Biopython was loaded.\n")
	pass

##
# Parse arguments from the command line
##

parser=argparse.ArgumentParser()
parser.add_argument("-i", "--input", help = "Input pairwise sequence alignment(s) in FASTA format.", type = str, nargs = "+", required = True)
parser.add_argument("-g", "--gap", help = "Symbol indicating InDels (the default is '-').", type = str, default = "-", required = False)
parser.add_argument("-m", "--missing", help = "Symbol indicating missing data (the default is '?').", type = str, default = "?", required = False)
parser.add_argument("-r", "--reference", help = "Indicates whether the reference sequence is the first (1) or second (2) entry in the pairwise alignment (default = 1).", type = int, default = 1, choices = [1, 2], required = False)
args = parser.parse_args()

##
# Define functions
##

def removeTrailingGaps(s):
	try:
		i = len(re.compile("^{}+".format(args.gap)).findall(s)[0])
	except:
		i = ""
	try:
		j = len(re.compile("{}+$".format(args.gap)).findall(s)[0])
	except:
		j = ""
	if i:
		s = i * args.missing + s[i : ]
	if j:
		s = s[ : -1 * j] + j * args.missing
	return s

def delGapOnly(sequences):
	blacklist = []
	for p in range(0, len(sequences[1])):
		i = str(sequences[1][p])
		j = str(sequences[2][p])
		if (i == args.gap and j == args.gap) or (i == args.missing and j == args.missing):
			blacklist.append(p)
	for p in sorted(blacklist)[::-1]:
		sequences[1] = sequences[1][:p] + sequences[1][p+1:]
		sequences[2] = sequences[2][:p] + sequences[2][p+1:]
	return sequences

def readFasta(fastaFile):
	sequences = {}
	headers = {}
	if(path.exists(fastaFile)):
		handle = open(fastaFile, "r")
		countEntries = 1
		for record in SeqIO.parse(handle, "fasta"):
			headers[countEntries] = record.description
			sequences[countEntries] = list(removeTrailingGaps(str(record.seq)))
			# sys.stdout.write(">{}\n{}\n".format(record.description, record.seq))
			countEntries += 1
		handle.close()
	return delGapOnly(sequences), headers

def maskReferenceSeq(sequence, gap):
	position = 1
	track = {}
	for i in range(0, len(sequence)):
		if(sequence[i] != gap):
			track[i] = position
			position += 1
		else:
			track[i] = gap
	# sys.stdout.write("//\n{}\n//".format(sequence))
	return track

def identicals(ref, seq, gap):
	identicals = {}
	blocks = {}
	position = 1
	last = 0
	length = 0
	for i in range(0, len(seq)):
		if(ref[i] != gap):
			if (seq[i] != gap) and (ref[i] != args.missing) and (seq[i] != args.missing) and (ref[i] == seq[i]):
				identicals[position] = ref[i]
				length += 1
				if last == 0:
					last = position
			else:
				if length != 0:
					blocks[last] = length
				length = 0
				last = 0
			position += 1
		else:
			if length != 0:
				blocks[last] = length
			length = 0
			last = 0
	return identicals, blocks

def reportIdenticals(max, identicalsDic, blocksDic, fileNumber, sources):

	sys.stderr.write("Percentage of identical sites in {}: {:.2%}\n".format(sources[fileNumber - 1], len(identicalsDic)/float(max)))

	for i in range(1, max + 1):
		if i in identicalsDic:
			sys.stdout.write("{}, {}, {}, {}, {}, {}, {}\n".format(fileNumber, i, sources[fileNumber - 1], "identical", "1", "1", identicalsDic[i]))
		else:
			sys.stdout.write("{}, {}, {}, {}, {}, {}, {}\n".format(fileNumber, i, sources[fileNumber - 1], "identical", "0", "0", "NA"))
		if i in blocksDic:
			sys.stdout.write("{}, {}, {}, {}, {}, {}, {}\n".format(fileNumber, i, sources[fileNumber - 1], "block", blocksDic[i], "1", "NA"))
		else:
			sys.stdout.write("{}, {}, {}, {}, {}, {}, {}\n".format(fileNumber, i, sources[fileNumber - 1], "block", "0", "0", "NA"))
	return

def getPatientData(fileList):
	sources = []
	for file in fileList:
		try:
			source = re.compile("^(.+?)[\.\_\-].*").findall(re.sub(".*\/", "", file))[0]
		except:
			sys.stderr.write("! ERROR: Problems parsing file name ({}) to extract source data. Make sure the file names start with the source ID followed by a period, an underscore, or a dash.\n".format(file))
			exit()
		else:
			sources.append(source)
	return sources

##
# Execute functions
##

sources = getPatientData(args.input)
if((not sources) or (len(sources) != len(args.input))):
	sys.stderr.write("! ERROR: Problems parsing source data. Please check arguments or input file names.\n")
	exit()

fileNumber = 0
sys.stdout.write("N, Position, Source, Type, Length, Count, Character\n")
for fastaFile in args.input:
	fileNumber += 1
	sequences, headers = readFasta(fastaFile)
	if(args.reference == 1):
		track = maskReferenceSeq(sequences[1], args.gap)
		identicalsDic, blocksDic = identicals(sequences[1], sequences[2], args.gap)
	else:
		track = maskReferenceSeq(sequences[2], args.gap)
		identicalsDic, blocksDic = identicals(sequences[2], sequences[1], args.gap)
	max = [track[i] for i in track if track[i] != args.gap][-1]
	reportIdenticals(max, identicalsDic, blocksDic, fileNumber, sources)

exit() # Quit this script
