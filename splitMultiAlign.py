#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# splitMultiAlign.py

# Split an alignment file with multiple entries into several pairwise alignment files.

##
# Import libraries
##

import argparse, re, sys
try:
	from Bio import SeqIO
except:
	sys.stderr.write("! ERROR: Could not load BioPython\n")
	exit()

##
# Parse arguments from the command line
##

parser=argparse.ArgumentParser()
parser.add_argument("-f", "--fasta", help = "Input file in FASTA format", type = str)
parser.add_argument("-r", "--reference", help = "Sequence ID (not description) for the reference sequence", type = str)
args = parser.parse_args()

##
# Define functions
##

def out(t):
	return sys.stdout.write(t + "\n")

def err(t):
	return sys.stderr.write(t + "\n")

def main(path, ref):
	sequence_dic = {}
	names_dic = {}
	for record in SeqIO.parse(path, "fasta"):
		id = record.id
		des = (record.description)
		seq = str(record.seq)
		if id == ref:
			refseq = seq
			refid = des
		else:
			sequence_dic[des] = seq
			names_dic[des] = id
	for des in sequence_dic:
		align = ">{}\n{}\n>{}\n{}\n".format(refid, refseq, des, sequence_dic[des])
		handle = open("{}.fasta".format(re.sub("[\_,;:\-\+\s ]","",names_dic[des])), "w")
		handle.write(align)
		handle.close()
	return

##
# Execute functions
##

main(args.fasta, args.reference) # This is the main fuction
exit() # Quit this script
