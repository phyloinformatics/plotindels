#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# parseTables.py

# Read the CSV output tables from parseAlignments and parseTranslations and report a summarized table.
# Columns: N, Position, Source, Type, Length, Count, Change
##
# Import libraries
##

import argparse, re, sys, csv

##
# Parse arguments from the command line
##

parser=argparse.ArgumentParser()
parser.add_argument("-n", "--nucl", help = "Table with insertions, deletions, and mutations (nucleotides)", type = str, default = "", required = False)
parser.add_argument("-a", "--amino", help = "Table with amino acid changes", type = str, default = "", required = False)
parser.add_argument("-s", "--start", help = "Start position. Use 1 to start at the first position (default = 1)", type = int, default = 1, required = False)
parser.add_argument("-e", "--end", help = "End position. Use -1 to start at the last position (default = -1)", type = int, default = -1, required = False)
args = parser.parse_args()

##
# Define functions
##

def out(t):
	return sys.stdout.write(t + "\n")

def err(t):
	return sys.stderr.write(t + "\n")

def parseTable(path, data, start, end):
	with open(path, newline='') as csvfile:
		reader = csv.DictReader(csvfile)
		fieldnames = reader.fieldnames
		for row in reader:
			row.update({fieldname.strip(): value.strip() for (fieldname, value) in row.items()})
			if (int(row["Position"]) >= start) and (int(row["Position"]) <= end or end == -1):
				if not row["Position"] in data:
					if row["Type"] in ["insertion", "deletion"] and int(row["Length"]) != 0:
						data[row["Position"]] = { "InDels,{}".format(row["Type"]) : [row["Source"]] }
					elif row["Type"] == "mutation" and row["Change"] != "NA":
						data[row["Position"]] = { "Nucleotides,{}".format(row["Change"].lower()) : [row["Source"]] }
					elif row["Type"] == "translation" and row["Change"] != "NA":
						data[row["Position"]] = { "Amino Acids,{}".format(row["Change"].upper()) : [row["Source"]] }
				else:
					if row["Type"] in ["insertion", "deletion"] and int(row["Length"]) != 0:
						if not row["Type"] in data[row["Position"]]:
							data[row["Position"]]["InDels,{}".format(row["Type"])] = [row["Source"]]
						else:
							data[row["Position"]]["InDels, {}".format(row["Type"])] += [row["Source"]]
					elif row["Type"] == "mutation" and row["Change"] != "NA":
						if not row["Change"].lower() in data[row["Position"]]:
							data[row["Position"]]["Nucleotides,{}".format(row["Change"].lower())] = [row["Source"]]
						else:
							data[row["Position"]]["Nucleotides,{}".format(row["Change"].lower())] += [row["Source"]]
					elif row["Type"] == "translation" and row["Change"] != "NA":
						if not row["Change"].upper() in data[row["Position"]]:
							data[row["Position"]]["Amino Acids,{}".format(row["Change"].upper())] = [row["Source"]]
						else:
							data[row["Position"]]["Amino Acids,{}".format(row["Change"].upper())] += [row["Source"]]
	return data

def report(data):
	out("Source,Position,Type,Change")
	for position in data:
		for change in data[position]:
			for source in data[position][change]:
				out("{},{},{}".format(source, position, change))
	return

##
# Execute functions
##

data = {}
if args.nucl:
	data = parseTable(args.nucl, data, args.start, args.end) # This is the main fuction
if args.amino:
	data = parseTable(args.amino, data, args.start, args.end) # This is the main fuction
report(data)

exit() # Quit this script
